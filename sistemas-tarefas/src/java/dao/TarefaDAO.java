package dao;

import entidade.Tarefa;
import util.Conexao;
import exception.ErroSistema;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author guigo
 */
public class TarefaDAO {
    
    public void salvar(Tarefa tarefa) throws ErroSistema{
        try {
            Connection conexao = Conexao.getConexao();
            PreparedStatement ps;
            if(tarefa.getId() == null){
                ps = conexao.prepareStatement("INSERT INTO `tarefa` (`nome`,`status`,`descricao`,`data`) VALUES (?,?,?,?)");
            } else {
                ps = conexao.prepareStatement("update tarefa set nome=?, status=?, descricao=?, data=? where id=?");
                ps.setInt(5, tarefa.getId());
            }
            ps.setString(1, tarefa.getNome());
            ps.setString(2, tarefa.getStatus());
            ps.setString(3, tarefa.getDescricao());
            ps.setDate(4, new Date(tarefa.getData().getTime()));
            ps.execute();
            Conexao.fecharConexao();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao tentar salvar!", ex);
        }
    }
    
    public void deletar(Integer idTarefa) throws ErroSistema{
        try {
            Connection conexao = Conexao.getConexao();
            PreparedStatement ps  = conexao.prepareStatement("delete from tarefa where id = ?");
            ps.setInt(1, idTarefa);
            ps.execute();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao deletar uma tarefa!", ex);
        }
    }
    
    public List<Tarefa> buscar() throws ErroSistema{
        try {
            Connection conexao = Conexao.getConexao();
            PreparedStatement ps = conexao.prepareStatement("select * from tarefa");
            ResultSet resultSet = ps.executeQuery();
            List<Tarefa> tarefas = new ArrayList<>();
            while(resultSet.next()){
                Tarefa tarefa = new Tarefa();
                tarefa.setId(resultSet.getInt("id"));
                tarefa.setNome(resultSet.getString("nome"));
                tarefa.setStatus(resultSet.getString("status"));
                tarefa.setDescricao(resultSet.getString("descricao"));
                tarefa.setData(resultSet.getDate("data"));
                tarefas.add(tarefa);
            }
            Conexao.fecharConexao();
            return tarefas;
            
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar as tarefas!",ex);
        }
    }
}
